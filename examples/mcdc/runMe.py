#!/usr/bin/env python3

# Thomas Wucher
# December 2019

# Simple BDD script to build a binary decision diagram for MCDC source code
# test coverage analysis

import sys
import pprint
sys.path.append("../../include/")
sys.path.append("../../../PBL/include/")
import BDD

if __name__ == "__main__":
    pp = pprint.PrettyPrinter(indent=4)

    #creating and initializing the BDD
    x = BDD.bdd_init("decision.txt")

    pp.pprint(x)

    #building the tree
    BDD.ite_build(x)

    BDD.dot_bdd(x, "dotfile.dot")
    print(BDD.all_sat(x))

    pp.pprint(x)
